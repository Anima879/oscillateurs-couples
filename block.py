class Block:
    """
        Class modélisant un block.
    """

    dt = 1
    dimension = (1000, 250)

    def __init__(self, toile, x, w, m, v):
        """
            Constructeur.
        :param toile: {Canvas} toile où afficher les objets.
        :param x: Abscisse du bords gauche de l'objet à t=0.
        :param w: Largeur de l'objet.
        :param m: Masse de l'objet.
        :param v: Vitesse de l'objet à t=0.
        """
        self.x = x
        self.w = w
        self.m = m
        self.v = v
        # Accélération à 0 à t=0
        self.a = 0
        self.toile = toile
        self.gobj = toile.create_rectangle(x, Block.dimension[1] / 2 - w / 2, x + w, Block.dimension[1] / 2 + w / 2,
                                           fill="red", outline='red')

        # Référence des objets {Ressort} à gauche et à droite de l'objet.
        self.ressort_g = None
        self.ressort_d = None

        self.lst_x_mem = []

    def update(self):
        """
            Mis-à-jour des attributs avec l'intégraton d'Euler au pas Block.dt
        :return:
        """
        self.a /= self.m
        self.v += self.a * Block.dt
        self.x += self.v * Block.dt
        self.toile.move(self.gobj, self.v * Block.dt, 0)
        self.a = 0
        self.lst_x_mem.append(self.x)

    def redraw(self):
        """
            Redessine l'objet en fonction de ses attributs
        :return: void
        """
        self.toile.delete(self.gobj)
        self.gobj = self.toile.create_rectangle(self.x, Block.dimension[1] / 2 - self.w / 2, self.x + self.w,
                                                Block.dimension[1] / 2 + self.w / 2,
                                                fill="red", outline='red')

    def collide(self, other):
        """
            Renvoie True si self est en collision avec other
        :param other: {Block}
        :return: {bool}
        """
        return not (self.x + self.w <= other.x or self.x > other.x + other.w)

    def bounce(self, other):
        """
            Rebond entre deux corps
        :param other: {Block}
        :return: {float}
        """
        sum_m = self.m + other.m
        dif_m = self.m - other.m
        new_v = ((dif_m / sum_m) * self.v) + ((2 * other.m / sum_m) * other.v)
        return new_v

    def set_ressort(self, gauche=None, droite=None):
        """
            Méthode utilisée pour lier des objets {Ressort}à l'instance.
        :param gauche:
        :param droite:
        :return:
        """
        if gauche is not None:
            self.ressort_g = gauche

        if droite is not None:
            self.ressort_d = droite
