/FR\

#Oscillateur couplé

Simulation d'un oscillateur couplé à deux masses égales avec 3 ressorts.
Exécuter `main`pour lancer le programme.

* Le bouton graphique n'est pas encore implémenté. Le graphique s'affiche à la fin.
* Au fil de la simulation (après 10 secondes), le temps se dilate (raison inconnue). Il faut relancer le programme pour supprimer cette dilatation.
* Tous les paramètres ne sont pas configurables.
* Les positions des masses sont par rapport l'origine à gauche (mur côté gauche à l'état initial).
