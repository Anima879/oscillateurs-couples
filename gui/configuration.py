import tkinter as tk
from gui.masse_config import MassConfig


class Configuration(tk.Frame):

    def __init__(self, root, **kwargs):
        tk.Frame.__init__(self, root, **kwargs)
        self.root = root
        self.grid()
        self.update()

        self.is_alive = True

        self.data = {}
        self.m1_frame = tk.LabelFrame(self, text='Masse 1', width=200, height=200)
        self.m1_frame.grid(column=0, row=0, padx=(10, 5), pady=(10, 10))

        self.m2_frame = tk.LabelFrame(self, text='Masse 2', width=200, height=200)
        self.m2_frame.grid(column=1, row=0, padx=(5, 10), pady=(10, 10))

        self.config_1 = MassConfig(self.m1_frame)
        self.config_2 = MassConfig(self.m2_frame)
        self.config_1.grid(padx=10, pady=10)
        self.config_2.grid(padx=10, pady=10)

        self.btn_validation = tk.Button(self, text='Valider', command=self.validation)
        self.btn_validation.grid(row=3, column=1, padx=10, pady=(0, 10), sticky=tk.E)

        self.update()

    def validation(self):
        self.data['Mass 1'] = {}
        self.data['Mass 1']['Position'] = self.config_1.position.get()
        self.data['Mass 1']['Vitesse'] = self.config_1.vitesse.get()
        self.data['Mass 1']['Masse'] = self.config_1.weight.get()

        self.data['Mass 2'] = {}
        self.data['Mass 2']['Position'] = self.config_2.position.get()
        self.data['Mass 2']['Vitesse'] = self.config_2.vitesse.get()
        self.data['Mass 2']['Masse'] = self.config_2.weight.get()
        print("In :", self.data)

        self.is_alive = False
        self.root.destroy()
