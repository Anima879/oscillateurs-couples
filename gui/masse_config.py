import tkinter as tk


class MassConfig(tk.Frame):

    def __init__(self, root, **kwargs):
        tk.Frame.__init__(self, root, kwargs)
        self.grid()

        # Variables :
        self.weight = tk.StringVar()
        self.vitesse = tk.StringVar()
        self.position = tk.StringVar()

        self.lbl_weight = tk.Label(self, text='Masse :')
        self.lbl_weight.grid(row=0, column=0)
        self.entry_weight = tk.Entry(self, textvariable=self.weight, width=5)
        self.entry_weight.grid(row=0, column=1)

        self.lbl_vit = tk.Label(self, text='Vitesse :')
        self.lbl_vit.grid(row=1, column=0, pady=5)
        self.entry_vit = tk.Entry(self, textvariable=self.vitesse, width=5)
        self.entry_vit.grid(row=1, column=1)

        self.lbl_pos = tk.Label(self, text='Position :')
        self.lbl_pos.grid(row=2, column=0)
        self.entry_pos = tk.Entry(self, textvariable=self.position, width=5)
        self.entry_pos.grid(row=2, column=1)


def main():
    root = tk.Tk()
    test = MassConfig(root, width=500, height=500)
    test.grid()

    root.mainloop()


if __name__ == '__main__': main()
