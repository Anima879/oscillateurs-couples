import tkinter as tk
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import threading


class GraphUi(tk.Frame):

    def __init__(self, root, **kwargs):
        # Initialisation par héritage :
        tk.Frame.__init__(self, root, **kwargs)
        self.grid()
        self.update()

        # Graphique:
        self.fig = Figure(figsize=(8, 3.5))
        self.ax1 = self.fig.add_subplot(211)
        self.ax2 = self.fig.add_subplot(212, sharex=self.ax1)
        self.ax1.set_xlabel("Temps")
        self.graph = FigureCanvasTkAgg(self.fig, root)
        self.graph_canvas = self.graph.get_tk_widget().grid()

    def g_update(self, lx1, lx2):
        self.ax1.clear()
        self.ax2.clear()
        self.ax1.set_xlabel("Temps")
        self.ax1.plot(lx1, 'g')
        self.ax2.plot(lx2, 'r')


class GraphThread(threading.Thread):

    def __init__(self, graph, m1, m2):
        threading.Thread.__init__(self)
        assert isinstance(graph, GraphUi)
        self.graph = graph
        self.m1 = m1
        self.m2 = m2
        self.lx1 = []
        self.lx2 = []

    def run(self):
        self.graph.g_update(self.lx1, self.lx2)
