import tkinter as tk
from gui.configuration import Configuration
import gui.graphique as gra


class Gui(tk.Frame):
    """
        Class formalisant l'affichage graphique principale.
    """

    def __init__(self, root, **kwargs):
        # Initialisation par héritage :
        tk.Frame.__init__(self, root, **kwargs)
        self.grid()
        self.update()

        # Flags :
        self.ended = False
        self.paused = False
        self.flag_reset = False
        self.s_graph = False

        # Variable :
        self.v_time = tk.StringVar()
        self.v_x1 = tk.StringVar()
        self.v_x2 = tk.StringVar()
        self.init_x1 = tk.StringVar()
        self.init_x2 = tk.StringVar()
        self.init_f = tk.StringVar()

        # Création de la toile :
        self.toile = tk.Canvas(self, bg='white', width=self.winfo_width(), height=self.winfo_height())
        self.toile.grid()

        # Label d'information :
        self.label_frame = tk.LabelFrame(self, height=50, padx=5, borderwidth=3, text="Contrôles",
                                         width=self.winfo_width())
        self.label_frame.grid(sticky=tk.W + tk.E)

        self.time = tk.Label(self.label_frame, textvariable=self.v_time)
        self.time.grid(rowspan=2, column=0, padx=(0, 20))

        self.x1 = tk.Label(self.label_frame, textvariable=self.v_x1)
        self.x1.grid(row=0, column=1)

        self.x2 = tk.Label(self.label_frame, textvariable=self.v_x2)
        self.x2.grid(row=1, column=1)

        # Boutons :
        self.quit_btn = tk.Button(self.label_frame, text='Quitter', command=self.stop)
        self.quit_btn.grid(row=0, rowspan=2, column=8, padx=(20, 0))

        self.btn_reset = tk.Button(self.label_frame, text='Reset', command=self.reset)
        self.btn_reset.grid(column=7, row=0, rowspan=2, padx=(20, 0))

        self.btn_show_graph = tk.Button(self.label_frame, text='Graphique', command=self.show_graph)
        self.btn_show_graph.grid(column=10, row=0, rowspan=2, padx=(20, 0))

        # Entrées conditions initiales :
        self.lbl_ex1 = tk.Label(self.label_frame, text='x1 initiale :')
        self.lbl_ex1.grid(column=2, row=0, padx=(20, 0))
        self.lbl_ex2 = tk.Label(self.label_frame, text='x2 initiale :')
        self.lbl_ex2.grid(column=2, row=1, padx=(20, 0))

        self.e1 = tk.Entry(self.label_frame, textvariable=self.init_x1, width=5)
        self.e1.grid(column=3, row=0)
        self.e2 = tk.Entry(self.label_frame, textvariable=self.init_x2, width=5)
        self.e2.grid(column=3, row=1)

        # Entrée force de frottements :
        self.lbl_f = tk.Label(self.label_frame, text='frottements :')
        self.lbl_f.grid(column=4, row=0, rowspan=2, padx=(20, 0))
        self.fe1 = tk.Entry(self.label_frame, textvariable=self.init_f, width=3)
        self.fe1.grid(column=5, row=0, rowspan=2)

        # Curseur de pas :
        self.pas_scale = tk.Scale(self.label_frame, orient='horizontal', from_=0, to=3, resolution=0.1, length=110,
                                  label='Pas :')
        self.pas_scale.grid(column=9, row=0, rowspan=2, padx=(25, 0))
        self.pas_scale.set(0)

        # Générateur :
        self.lbl_gen = tk.Label(self.label_frame, text='Générateur :')
        self.lbl_gen.grid(row=0, rowspan=2, column=11, padx=(10, 0))
        self.pulse_scale = tk.Scale(self.label_frame, orient='vertical', from_=0, to=20, resolution=1, length=50,
                                    label='Pulsation', sliderlength=15)
        self.pulse_scale.grid(row=0, rowspan=2, column=12)
        self.amp_scale = tk.Scale(self.label_frame, orient='vertical', from_=0, to=100, resolution=5, length=50,
                                    label='Amplitude', sliderlength=15)
        self.amp_scale.grid(row=0, rowspan=2, column=13)

        # Graphique:
        # self.w_graph = gra.GraphUi(self)

        # Configuration :
        self.w_config = None

    def stop(self):
        self.ended = True

    def reset(self):
        if not self.flag_reset:
            self.flag_reset = True
        else:
            self.flag_reset = False

        # if self.w_config is None:
        #     window = tk.Tk()
        #     window.title('Configuration')
        #     self.w_config = Configuration(window)
        #     self.w_config.grid()
        #     window.update()
        #
        # self.w_config.update()
        # while self.w_config.is_alive:
        #     self.w_config.update()
        #
        # print("Out :", self.w_config.data)
        # self.w_config = None

    def show_graph(self):
        if self.btn_show_graph['relief'] == 'raised':
            self.btn_show_graph['relief'] = 'sunken'
            self.s_graph = True
        else:
            self.btn_show_graph['relief'] = 'raised'
            self.s_graph = False
