import tkinter as tk
from block import Block
from ressort import Ressort
from wall import Wall
import matplotlib.pyplot as plt
import time
import gui.root as gui

__VERSION__ = "V0.4"
__AUTHOR__ = "Eloi Mahé"
__LUD__ = "31/05/2019"


def force_rappel(ressort):
    """
        Calcul la force de rappel du ressort sur l'objet
    :param ressort:
    :return:
    """
    return ressort.k0 * (ressort.l - ressort.l0)


def force_frottements(coef, vitesse):
    """
    :param coef: Coefficient de la force de frottement.
    :param vitesse: vitesse du mobile.
    :return:
    """
    return - coef * vitesse


def f(x):
    """
        Translation sur x
    :param x:
    :return:
    """
    return x + 100


def rev_f(x):
    """
        Translation selon -x
    :param x:
    :return:
    """
    return x - 100


def anim_loop(master, m_1, m_2, r_0, r_1, r_2, mur_1, mur_2):
    """
        Boucle d'animation de la simulation.
    :param master:
    :param m_1:
    :param m_2:
    :param r_0:
    :param r_1:
    :param r_2:
    :param mur_1:
    :param mur_2:
    :return:
    """
    list_x1 = []
    list_x2 = []
    list_time = []
    coef = 0
    sim_clock = 0
    frame_count = 0

    while not master.ended:
        start_time = time.time()
        Block.dt = master.pas_scale.get()
        mur_1.amp = master.amp_scale.get()
        mur_1.w = master.pulse_scale.get()
        while master.paused:
            master.update()

        # Reset des valeurs
        if master.flag_reset:
            start_time = time.time()
            # Un champs vide lève une erreur, on la contourne donc.
            try:
                m_1.x = f(float(master.init_x1.get()))
                m_1.v = 0
                m_1.redraw()
            except ValueError:
                pass
            try:
                m_2.x = f(float(master.init_x2.get()))
                m_2.v = 0
                m_2.redraw()
            except ValueError:
                pass
            try:
                coef = float(master.init_f.get())
            except ValueError:
                pass
            master.flag_reset = False
            Block.dt = 1
            master.pas_scale.set(1)
            master.update()

        # Calcule des forces :
        # Masse 1 :
        m_1.a -= force_rappel(r_0)
        m_1.a += force_rappel(r_1)
        m_1.a += force_frottements(coef, m_1.v)
        # Masse 2 :
        m_2.a -= force_rappel(r_1)
        m_2.a += force_rappel(r_2)
        m_2.a += force_frottements(coef, m_2.v)

        # Mis-à-jour des objets :
        mur_1.update(sim_clock)
        m_1.update()
        m_2.update()
        r_0.update()
        r_1.update()
        r_2.update()

        # MàJ de l'horloge :
        sim_clock += (time.time() - start_time) * Block.dt

        # MàJ des variables de contrôle
        master.v_time.set("Temps : %.2f s" % sim_clock)
        master.v_x1.set("x1 : %.0f" % rev_f(m_1.x))
        master.v_x2.set("x2 : %.0f" % rev_f(m_2.x))

        # MàJ de la fenêtre graphique
        master.update()
        frame_count += 1

        # Mémoire :
        if frame_count % 2 == 0:
            list_x1.append(m_1.x)
            list_x2.append(m_2.x)
            list_time.append(sim_clock)

        print('Frame :', frame_count)

    return list_x1, list_x2, list_time


def main():
    dimension = (1000, 250)
    L = 800
    Block.dimension = dimension
    Ressort.dimension = dimension
    Wall.dimension = dimension

    root = tk.Tk()
    root.title('Oscillateurs couplés ' + __VERSION__)
    master = gui.Gui(root, width=dimension[0], height=dimension[1], bg='white')
    master.grid()

    m_1 = Block(master.toile, f(233), 30, 10, 0)
    m_2 = Block(master.toile, f(566), 30, 10, 0)

    mur_1 = Wall(master.toile, 100, 100)
    mur_2 = Wall(master.toile, 100 + L, 100)
    mur_1.draw()
    mur_2.draw()

    r_0 = Ressort(master.toile, 0.03, 233, mur_1, m_1, 6)
    r_1 = Ressort(master.toile, 0.007, 303, m_1, m_2, 6, offset_1=m_1.w)
    r_2 = Ressort(master.toile, 0.03, 204, m_2, mur_2, 6, offset_1=m_2.w)
    r_0.draw()
    r_1.draw()
    r_2.draw()

    m_1.set_ressort(gauche=r_0, droite=r_1)
    m_2.set_ressort(gauche=r_1, droite=r_2)

    # Boucle d'animation
    list_x1, list_x2, list_time = anim_loop(master, m_1, m_2, r_0, r_1, r_2, mur_1, mur_2)

    print(len(list_time))
    print(len(list_x1))
    print(len(list_x2))
    plt.plot(list_time, list_x1)
    plt.plot(list_time, list_x2)
    plt.show()


if __name__ == '__main__': main()
