from math import cos


class Wall:
    dimension = (1000, 250)

    def __init__(self, toile, x, h):
        self.toile = toile
        self.x0 = x
        self.x = x
        self.v = 0
        self.h = h / 2
        self.gobj = None
        self.amp = 10
        self.phase = 0
        self.w = 20

    def draw(self):
        self.gobj = self.toile.create_rectangle(self.x, Wall.dimension[1] / 2 - self.h, self.x + 1,
                                                Wall.dimension[1] / 2
                                                + self.h, fill='white', outline='black')

    def update(self, t):
        self.x = self.x0 + self.amp * cos(self.w * t + self.phase)
        self.toile.delete(self.gobj)
        self.gobj = self.toile.create_rectangle(self.x, Wall.dimension[1] / 2 - self.h, self.x + 1,
                                                Wall.dimension[1] / 2
                                                + self.h, fill='white', outline='black')
