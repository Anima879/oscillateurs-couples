class Ressort:
    """
        Class modélisant un ressort.
    """
    dimension = (1000, 250)
    dt = 1

    def __init__(self, toile, k0, l0, obj_1, obj_2, density, offset_1=0, offset_2=0):
        """
            Constructeur.
        :param toile: {Canvas} toile où afficher les objets.
        :param k0: Raideur du ressort
        :param l0: Longueur à ide du ressort
        :param obj_1: Objet lié à gauche
        :param obj_2: Objet lié à droite
        :param density: Densité de points du ressort pour l'affichage graphique
        :param offset_1: Décalage lié à la largeur de l'objet 1
        :param offset_2: Décalage lié à la largeur de l'objet 2
        """
        # Attributs graphiques :
        self.x1 = obj_1.x + offset_1
        self.x2 = obj_2.x + offset_2
        self.obj_1 = obj_1
        self.obj_2 = obj_2
        self.toile = toile
        self.density = density
        self.list_line = []
        self.offset_1 = offset_1
        self.offset_2 = offset_2

        # Attributs physiques :
        self.k0 = k0
        self.l0 = l0
        self.l = abs(self.x2 - self.x1)

    def draw(self):
        """
            Affichage du ressort.
        :return:
        """
        y = Ressort.dimension[1] / 2
        d = 20
        h = 15
        density = self.density

        distance = self.x2 - self.x1 - 2 * d
        dx = distance / density
        a = self.toile.create_line(self.x1, y, self.x1 + d, y)
        self.list_line.append(a)
        a = self.toile.create_line(self.x2, y, self.x2 - d, y)
        self.list_line.append(a)
        a = self.toile.create_line(self.x1 + d, y, self.x1 + d + dx, y - h)
        self.list_line.append(a)
        if density % 2 == 0:
            a = self.toile.create_line(self.x2 - d, y, self.x2 - d - dx, y - h)
        else:
            a = self.toile.create_line(self.x2 - d, y, self.x2 - d - dx, y + h)
        self.list_line.append(a)
        density = density - 2
        if density < 0:
            density = 0

        point = self.x1 + d + dx
        haut = False
        for i in range(0, density):
            if haut:
                a = self.toile.create_line(point, y + h, point + dx, y - h)
                haut = False
            else:
                a = self.toile.create_line(point, y - h, point + dx, y + h)
                haut = True
            point += dx
            self.list_line.append(a)

    def update(self):
        """
            Le ressort étant trop complexe à animer par translation,
            on se contente de l'effacer et de le dessiner à nouveau.
        :return:
        """
        self.x1 = self.obj_1.x + self.offset_1
        self.x2 = self.obj_2.x + self.offset_2
        self.l = abs(self.x2 - self.x1)

        for elt in self.list_line:
            self.toile.delete(elt)

        self.draw()
